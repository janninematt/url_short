package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/janninematt/url_short"
)

func main() {

	yamlFile := flag.String("yaml", "", "yaml file path")
	jsonFile := flag.String("json", "", "json file path")
	flag.Parse()

	var format, path string

	switch {
	case *yamlFile != "":
		path = *yamlFile
		format = "yaml"
	case *jsonFile != "":
		path = *jsonFile
		format = "json"
	default:
		flag.Usage()
		return
	}

	paths, err := urlshort.LoadPathsFromFile(path, format)
	if err != nil {
		log.Fatalf("could not parse file: %v", err)
	}
	fmt.Printf("paths: %v\n", paths)

	router := make(map[string]string)
	for _, path := range paths {
		router[path.Path] = path.URL
	}

	mux := defaultMux()
	mapHandler := urlshort.MapHandler(router, mux)
	fmt.Println("Starting the server on :8080")
	http.ListenAndServe(":8080", mapHandler)
}

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)
	return mux
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, world!")
}
