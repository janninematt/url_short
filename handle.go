package urlshort

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"gopkg.in/yaml.v2"
)

// MapHandler will return an http.HandlerFunc (which also
// implements http.Handler) that will attempt to map any
// paths (keys in the map) to their corresponding URL (values
// that each key in the map points to, in string format).
// If the path is not provided in the map, then the fallback
// http.Handler will be called instead.
func MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	//	TODO: Implement this...
	return func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path
		if dest, ok := pathsToUrls[path]; ok {
			http.Redirect(w, r, dest, http.StatusFound)
			return
		}
		fallback.ServeHTTP(w, r)
	}
}

// YAMLHandler will parse the provided YAML and then return
// an http.HandlerFunc (which also implements http.Handler)
// that will attempt to map any paths to their corresponding
// URL. If the path is not provided in the YAML, then the
// fallback http.Handler will be called instead.
//
// YAML is expected to be in the format:
//
//     - path: /some-path
//       url: https://www.some-url.com/demo
//
// The only errors that can be returned all related to having
// invalid YAML data.
//
// See MapHandler to create a similar http.HandlerFunc via
// a mapping of paths to urls.
// func YAMLHandler(yml []byte, fallback http.Handler) (http.HandlerFunc, error) {
// 	// TODO: Implement this...
// 	return nil, nil
// }

func YAMLHandler(yaml []byte, fallback http.Handler) (http.HandlerFunc, error) {
	parsedYaml, err := parseYAML(yaml)
	if err != nil {
		return nil, err
	}
	pathMap := buildMap(parsedYaml)
	return MapHandler(pathMap, fallback), nil
}

func buildMap(paths []pathURL) map[string]string {
	route := make(map[string]string)
	for _, p := range paths {
		route[p.Path] = p.URL
	}
	return route
}

func parseYAML(data []byte) ([]pathURL, error) {
	var paths []pathURL
	if err := yaml.Unmarshal(data, &paths); err != nil {
		return nil, err
	}
	return paths, nil
}

func LoadPathsFromFile(path string, format string) ([]pathURL, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("could not open file: %v", err)
	}
	defer f.Close()

	var paths []pathURL

	switch format {
	case "yaml":
		if err := yaml.NewDecoder(f).Decode(&paths); err != nil {
			return nil, fmt.Errorf("could not parse yaml: %v", err)
		}
	case "json":
		if err := json.NewDecoder(f).Decode(&paths); err != nil {
			return nil, fmt.Errorf("could not parse json: %v", err)
		}
	default:
		log.Fatalf("not support format: %s", format)
	}
	return paths, nil
}

/*
- path: /urlshort
  url: https://github.com/gophercises/urlshort
- path: /urlshort-final
  url: https://github.com/gophercises/urlshort/tree/solution
*/
type pathURL struct {
	Path string `yaml:"path" json:"path"`
	URL  string `yaml:"url" json:"url"`
}
